/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package romannumberal;

import java.util.Scanner;

/**
 *
 * @author brian
 */
public class RomanNumberal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
     Scanner scan = new Scanner ( System.in ) ;
    
    System.out.print ( "Please enter a Roman numeral from the following set : " 
                        + "I, V, X, L, C, D, and M >" ) ;
    char numeral = scan.next().toUpperCase().charAt(0) ;
    
    boolean flag = true ;
    
    String quant ;
    
    switch ( numeral )
    {
      case 'I' :
        quant = "one" ;
        break ;
      case 'V' :
        quant = "five" ;
        break ;
      case 'X' :
        quant = "ten" ;
        break ;
      case 'L' :
        quant = "fifty" ;
        break ;
      case 'C' :
        quant = "one-hundred" ;
        break ;
      case 'D' :
        quant = "five-hundred" ;
        break ;
      case 'M' :
        quant = "one-thousand" ;
        break;
      default :
        quant = "is not a valid Roman numeral" ;
        flag = false;
    }
    
    if ( flag )
    {
      System.out.println ( "Roman numeral \"" + numeral + "\" represents \""
                            + quant + "\"." ) ;
    }
    else
    {
      System.out.println ( "\"" + numeral + "\" " + quant + "." ) ;
    }
    }
    
}
